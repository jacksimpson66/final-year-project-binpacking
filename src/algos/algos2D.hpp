#ifndef ALGOS2D_HPP
#define ALGOS2D_HPP

#include "..\\Binpack_lib\\application.hpp"
#include "..\\Binpack_lib\\instance.hpp"
#include "..\\Binpack_lib\\bins.hpp"

// #include "application.hpp"
// #include "instance.hpp"
// #include "bins.hpp"

// Base class of AlgoFit tailored for 2D bin packing
// With placeholder functions to sort the items, sort the bins
// and determine whether an items can be placed in a bin
class AlgoFit2D
{
public:
    AlgoFit2D(const Instance2D &instance);
    virtual ~AlgoFit2D();

    bool isSolved() const;
    int getSolution() const;
    const BinList2D& getBins() const;
    BinList2D getBinsCopy() const;
    const AppList2D& getApps() const;
    const int getBinCPUCapacity() const;
    const int getBinMemCapacity() const;
    const std::string& getInstanceName() const;

    void setSolution(BinList2D& bins);
    void clearSolution();

    const int getFinalReplicas() const;
    const int getFinalCPUDemand() const;
    const int getFinalMemDemand() const;
    const int getFinalBins() const;

    void updatePQCheckMeasures(Application2D* app);

    void freezePQMeasures();


    //void getBinsUsage(float &cpu_usage, float &mem_usage) const;

    int solveInstance(int hint_nb_bins = 0, int flag = 0, int num_bins = 10000000);
    int solvePerBatch(int batch_size, int hint_nb_bins = 0);
private:
    virtual void createNewBin(); // Open a new empty bin
    virtual void allocateBatch(AppList2D::iterator first_app, AppList2D::iterator end_batch);
    virtual void solveBatchedBins(int hint_nb_bins);

    // These are the 4 methods each variant of the Fit algo should implement
    virtual void sortBins() = 0;
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it) = 0;
    virtual bool checkItemToBin(Application2D* app, Bin2D* bin) const = 0;
    virtual void addItemToBin(Application2D* app, int replica_id, Bin2D* bin) = 0;

protected:
    std::string instance_name;
    int bin_cpu_capacity;
    int bin_mem_capacity;
    int total_replicas;
    int sum_cpu;
    int sum_mem;
    float norm_sum_cpu;
    float norm_sum_mem;
    int next_bin_index;
    int curr_bin_index;
    AppList2D apps;
    BinList2D bins;
    bool solved;

    int remaining_replicas;
    int remaining_cpu_demand;
    int remaining_mem_demand;

    int final_replicas;
    int final_cpu;
    int final_mem;
    int final_bins;

    int max_bins;
};

// Creator of AlgoFit2D variant w.r.t. given algo_name
AlgoFit2D* createAlgo2D(const std::string& algo_name, const Instance2D &instance);


/* ================================================ */
/* ================================================ */
/* ================================================ */
/************ First Fit Affinity *********/
class Algo2DFF : public AlgoFit2D
{
public: 
    Algo2DFF(const Instance2D &instance);
protected:
    virtual void solveBatchedBins(int hint_nb_bins);
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual void sortBins();
    virtual bool checkItemToBin(Application2D* app, Bin2D* bin) const;
    virtual void addItemToBin(Application2D* app, int replica_id, Bin2D* bin);
};


/************ First Fit Decreasing Degree Affinity *********/
class Algo2DFFDDegree : public Algo2DFF
{
public:
    Algo2DFFDDegree(const Instance2D &instance);
private:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};



/************ First Fit Decreasing Average Affinity *********/
class Algo2DFFDAvg : public Algo2DFF
{
public: 
    Algo2DFFDAvg(const Instance2D &instance);
private:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};


/************ First Fit Decreasing Average with Exponential Weights Affinity *********/
class Algo2DFFDAvgExpo : public Algo2DFF
{
public:
    Algo2DFFDAvgExpo(const Instance2D &instance);
private:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};


/************ First Fit Decreasing Max Affinity *********/
class Algo2DFFDMax : public Algo2DFF
{
public: 
    Algo2DFFDMax(const Instance2D &instance);
private:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};


/************ First Fit Decreasing Surrogate Affinity *********/
class Algo2DFFDSurrogate : public Algo2DFF
{
public: 
    Algo2DFFDSurrogate(const Instance2D &instance);

protected:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};


/************ First Fit Decreasing Extended Sum Affinity *********/
class Algo2DFFDExtendedSum : public Algo2DFF
{
public:
    Algo2DFFDExtendedSum(const Instance2D &instance);

protected:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};



/* ================================================ */
/* ================================================ */
/* ================================================ */
/************ Best Fit Decreasing Average Affinity *********/
class Algo2DBFDAvg : public AlgoFit2D
{
public:
    Algo2DBFDAvg(const Instance2D &instance);
private:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual void sortBins();
    virtual bool checkItemToBin(Application2D* app, Bin2D* bin) const;
    virtual void addItemToBin(Application2D* app, int replica_id, Bin2D* bin);

    virtual void updateBinMeasure(Bin2D* bin);
};

/************ Best Fit Decreasing Max Affinity *********/
class Algo2DBFDMax : public Algo2DBFDAvg
{
public:
    Algo2DBFDMax(const Instance2D &instance);
private:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual void updateBinMeasure(Bin2D* bin);
};


/************ Best Fit Decreasing Average with Exponential Weights Affinity *********/
class Algo2DBFDAvgExpo : public Algo2DBFDAvg
{
public:
    Algo2DBFDAvgExpo(const Instance2D &instance);
private:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual void sortBins();
    virtual void createNewBin();
    virtual void addItemToBin(Application2D *app, int replica_id, Bin2D *bin);
    virtual void updateBinMeasure(Bin2D* bin);
protected:
    int total_residual_cpu;
    int total_residual_mem;
};



/************ Best Fit Decreasing Surrogate Affinity *********/
class Algo2DBFDSurrogate : public Algo2DBFDAvgExpo
{
public:
    Algo2DBFDSurrogate(const Instance2D &instance);

protected:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual void sortBins();
    virtual void updateBinMeasure(Bin2D* bin);
};

/************ Best Fit Decreasing Extended Sum Affinity *********/
class Algo2DBFDExtendedSum : public Algo2DBFDAvgExpo
{
public:
    Algo2DBFDExtendedSum(const Instance2D &instance);

protected:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual void sortBins();
    virtual void updateBinMeasure(Bin2D* bin);
};

/* ================================================ */
/* ================================================ */
/* ================================================ */
/************ Worst Fit Decreasing Avg Affinity *********/
class Algo2DWFDAvg : public Algo2DBFDAvg
{
public:
    Algo2DWFDAvg(const Instance2D &instance);
private:
    virtual void sortBins();
};

/************ Worst Fit Decreasing Max Affinity *********/
class Algo2DWFDMax : public Algo2DBFDMax
{
public:
    Algo2DWFDMax(const Instance2D &instance);
private:
    virtual void sortBins();
};

/************ Worst Fit Decreasing AvgExpo Affinity *********/
class Algo2DWFDAvgExpo : public Algo2DBFDAvgExpo
{
public:
    Algo2DWFDAvgExpo(const Instance2D &instance);
private:
    virtual void sortBins();
};

/************ Worst Fit Decreasing Surrogate Affinity *********/
class Algo2DWFDSurrogate : public Algo2DBFDSurrogate
{
public:
    Algo2DWFDSurrogate(const Instance2D &instance);
private:
    virtual void sortBins();
};

/************ Worst Fit Decreasing ExtendedSum Affinity *********/
class Algo2DWFDExtendedSum : public Algo2DBFDExtendedSum
{
public:
    Algo2DWFDExtendedSum(const Instance2D &instance);
private:
    virtual void sortBins();
};


/* ================================================ */
/* ================================================ */
/* ================================================ */
/************ Medea Node Count Affinity *********/
class Algo2DNodeCount : public AlgoFit2D
{
public:
    Algo2DNodeCount(const Instance2D &instance);
protected:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual void sortBins();
    virtual bool checkItemToBin(Application2D* app, Bin2D* bin) const;
    virtual void addItemToBin(Application2D* app, int replica_id, Bin2D* bin);

private:
    virtual void allocateBatch(AppList2D::iterator first_app, AppList2D::iterator end_batch);
};




/* ================================================ */
/* ================================================ */
/* ================================================ */
/********* Bin Centric FFD DotProduct ***************/
class Algo2DBinFFDDotProduct : public Algo2DFF
{
public:
    Algo2DBinFFDDotProduct(const Instance2D &instance);

    //virtual int solveInstance(int hint_lower_bound = 0);
private:
    virtual void allocateBatch(AppList2D::iterator first_app, AppList2D::iterator end_batch);
protected:
    virtual Bin2D* createNewBinRet();
    virtual bool isBinFilled(Bin2D* bin);
    virtual void computeMeasures(AppList2D::iterator start_list, AppList2D::iterator end_list, Bin2D* bin);
};


/********* Bin Centric FFD L2Norm ***************/
class Algo2DBinFFDL2Norm : public Algo2DBinFFDDotProduct
{
public:
    Algo2DBinFFDL2Norm(const Instance2D &instance);

protected:
    virtual void computeMeasures(AppList2D::iterator start_list, AppList2D::iterator end_list,  Bin2D* bin);
};


/********* Bin Centric FFD Fitness ***************/
class Algo2DBinFFDFitness : public Algo2DBinFFDDotProduct
{
public:
    Algo2DBinFFDFitness(const Instance2D &instance);

protected:
    virtual void computeMeasures(AppList2D::iterator start_list, AppList2D::iterator end_list,  Bin2D* bin);

    virtual Bin2D* createNewBinRet();
    virtual void addItemToBin(Application2D *app, int replica_id, Bin2D *bin);
    int total_residual_cpu;
    int total_residual_mem;
};

/* ================================================ */
/* ================================================ */
/* ================================================ */
/*********** Spread replicas Worst Fit Avg **********/
class Algo2DSpreadWFDAvg : public AlgoFit2D
{
public:
    Algo2DSpreadWFDAvg(const Instance2D &instance);

    virtual int solveInstanceSpread(int LB_bins, int UB_bins);
// maybe need to remove protected:
    bool trySolve(int nb_bins); // Try to find a solution with the given bins
private:

    virtual void createBins(int nb_bins);
    virtual void updateBinMeasure(Bin2D* bin);
    virtual void updateBinMeasures();          // If all bins need to be updated

    virtual void allocateBatch(AppList2D::iterator first_app, AppList2D::iterator end_batch);

    virtual void sortBins();
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual bool checkItemToBin(Application2D* app, Bin2D* bin) const;
    virtual void addItemToBin(Application2D* app, int replica_id, Bin2D* bin);
};

/*********** Spread replicas Worst Fit Max **********/
class Algo2DSpreadWFDMax : public Algo2DSpreadWFDAvg
{
public:
    Algo2DSpreadWFDMax(const Instance2D &instance);

private:
    virtual void updateBinMeasure(Bin2D* bin);
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};


/*********** Spread replicas Worst Fit AvgExpo **********/
class Algo2DSpreadWFDAvgExpo : public Algo2DSpreadWFDAvg
{
public:
    Algo2DSpreadWFDAvgExpo(const Instance2D &instance);

private:
    virtual void createBins(int nb_bins);
    virtual void updateBinMeasure(Bin2D* bin);
    virtual void updateBinMeasures();
    virtual void addItemToBin(Application2D *app, int replica_id, Bin2D *bin);
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);

protected:
    int total_residual_cpu;
    int total_residual_mem;
};

/*********** Spread replicas Worst Fit Surrogate **********/
class Algo2DSpreadWFDSurrogate : public Algo2DSpreadWFDAvgExpo
{
public:
    Algo2DSpreadWFDSurrogate(const Instance2D &instance);

private:
    virtual void updateBinMeasure(Bin2D* bin);
    virtual void updateBinMeasures();
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};


/*********** Spread replicas Worst Fit Extended Sum **********/
class Algo2DSpreadWFDExtendedSum : public Algo2DSpreadWFDAvgExpo
{
public:
    Algo2DSpreadWFDExtendedSum(const Instance2D &instance);

private:
    virtual void updateBinMeasure(Bin2D* bin);
    virtual void updateBinMeasures();
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
};



/* ================================================ */
/* ================================================ */
/* ================================================ */
/**** A variant of SpreadWFD algorithms *************/
class Algo2DRefineWFDAvg : public Algo2DSpreadWFDAvg
{
public:
    Algo2DRefineWFDAvg(const Instance2D &instance, const float ratio);

    virtual int solveInstanceSpread(int LB_bins, int UB_bins);

private:
    float ratio_refinement;
};




Algo2DSpreadWFDAvg* createSpreadAlgo(const std::string &algo_name, const Instance2D &instance);

//MY CODE
//Class to contain max matching value and app and bin that it applies to
class MaxMatchValue {
    private:
        float value;
        Application2D* app;
        Bin2D* bin;
    public:
        MaxMatchValue();
        const float getValue() const;
        Application2D* getApp() const;
        Bin2D* getBin() const;
        void setValue(float val);
        void setApp(Application2D* app);
        void setBin(Bin2D* bin);
};

class AlgoAppNodeMatch2D : public AlgoFit2D
{
public:
    AlgoAppNodeMatch2D(const Instance2D &instance);
    MaxMatchValue getMaxMatchValue();
private:
    virtual void sortApps(AppList2D::iterator first_app, AppList2D::iterator end_it);
    virtual void sortBins();
    virtual bool checkItemToBin(Application2D* app, Bin2D* bin) const;
    virtual void addItemToBin(Application2D* app, int replica_id, Bin2D* bin);
    virtual void allocateBatch(AppList2D::iterator first_app, AppList2D::iterator end_batch);
    virtual float computeAppNodeMatchingScore(Application2D* app, Bin2D* bin) = 0;
    void solveNodeCentricApproach(AppList2D& fully_allocated_apps, AppList2D& unallocated_apps, 
        std::unordered_map<std::string, int>& next_id_replicas, int nb_apps);
    void resetMeasureValues();
    void solveBinBatch();
    virtual void solveBatchedBins(int hint_nb_bins);
protected:
    MaxMatchValue max_value;
    //T'h from paper in both h
    float total_residual_mem_capacity;
    float total_residual_cpu_capacity;
    //R i rem from paper
    float total_remaining_replicas;
    //T h rem
    float total_remaining_replica_cpu_demand;
    float total_remaining_replica_mem_demand;
    //T h
    float total_replica_cpu_demand;
    float total_replica_mem_demand;
    //testing purposes
    // float total_bin_mem_used;
    // float total_bin_cpu_used;
    // float total_replica_mem_used;
    // float total_replica_cpu_used;
    // float total_bin_mem_used_TEST;
    // float total_bin_cpu_used_TEST;
    AppList2D unallocated_apps;
    AppList2D fully_allocated_apps;
    std::unordered_map<std::string, int> next_id_replicas; 
};

//DotProduct scoring mechanism
class AlgoAppNodeMatch2DDP : public AlgoAppNodeMatch2D {
    public:
        AlgoAppNodeMatch2DDP(const Instance2D &instance);
    private:
        virtual float computeAppNodeMatchingScore(Application2D* app, Bin2D* bin);
};

//NormalisedDotProduct scoring mechanism
class AlgoAppNodeMatch2DNormDP : public AlgoAppNodeMatch2D {
    public:
        AlgoAppNodeMatch2DNormDP(const Instance2D &instance);
    private:
        virtual float computeAppNodeMatchingScore(Application2D* app, Bin2D* bin);
};

//DynamicNormalisedDotProduct scoring mechanism
class AlgoAppNodeMatch2DDynamicNormDP : public AlgoAppNodeMatch2D {
    public:
        AlgoAppNodeMatch2DDynamicNormDP(const Instance2D &instance);
    private:
        virtual float computeAppNodeMatchingScore(Application2D* app, Bin2D* bin);
};

//L2Norm scoring mechanism
class AlgoAppNodeMatch2DL2Norm : public AlgoAppNodeMatch2D {
    public:
        AlgoAppNodeMatch2DL2Norm(const Instance2D &instance);
    private:
        virtual float computeAppNodeMatchingScore(Application2D* app, Bin2D* bin);
};

//TightFill scoring mechanism
class AlgoAppNodeMatch2DTightFill : public AlgoAppNodeMatch2D {
    public:
        AlgoAppNodeMatch2DTightFill(const Instance2D &instance);
    private:
        virtual float computeAppNodeMatchingScore(Application2D* app, Bin2D* bin);
};

#endif // ALGOS2D_HPP
