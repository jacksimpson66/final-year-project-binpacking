#include "application.hpp"
#include "instance.hpp"
#include "lower_bounds.hpp"
#include "..\\algos\\algos2D.hpp"

#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;
using namespace std::chrono;

//function to run non spreading algorithms and return string of running duration
string run_non_spread_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + int(0.05*LB);
    
    string row = "";
    int sol;
    for (const string & algo_name : list_algos)
    {
        //solve instance with each algo and calculate running time
        AlgoFit2D * algo = createAlgo2D(algo_name, instance);
        if (algo != nullptr)
        {
            cout << "Running for " << algo_name << endl;
            auto start = high_resolution_clock::now();
            sol = algo->solveInstance(hint_bin);
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<milliseconds>(stop - start);

            row.append("," + to_string((float)duration.count() / 1000));
            delete algo;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
    return row;
}

//function that runs spread algorithms and return string of running duration
string run_spread_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    string row = "";
    int sol;

    int hint_bin = LB + int(0.05*LB);

    // Always take solution of FirstFit as upper bound input
    AlgoFit2D* algoFF = createAlgo2D("2DFF", instance);
    int UB = algoFF->solveInstance(hint_bin);
    delete algoFF;

    for (const string & algo_name : list_algos)
    {
        Algo2DSpreadWFDAvg * algo = createSpreadAlgo(algo_name, instance);
        if (algo != nullptr)
        {
            cout << "Running for " << algo_name << endl;
            auto start = high_resolution_clock::now();
            sol = algo->solveInstanceSpread(LB, UB);
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<seconds>(stop - start);

            row.append("," + to_string((float)duration.count() / 1000));
            delete algo;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
    return row;
}

//function that finds optimal number of bins for ANM
int solve_binary_search(string algo_name, const Instance2D & instance, int l, int r)
{
    int curr_sol;
    int one_less_bin_sol;

    AlgoFit2D* algo = createAlgo2D(algo_name, instance);
    /*
    Using best current result as an upper bound and the LB as the lower bound, see if any amount of 
    preactivated nodes/bins results in a better approximate solution.
    Executed using a recursive approach.
    */
    if (r > l)
    {
        if (r == l)
        {
            return algo->solveInstance(r);
        }
        int mid = (l + (r - 1)) / 2;
        auto start = high_resolution_clock::now();
        curr_sol = algo->solveInstance(mid);
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(stop - start);
        one_less_bin_sol = algo->solveInstance(mid - 1);
        if (curr_sol == mid && one_less_bin_sol > mid)
        {
            return mid;
        }
        if (curr_sol > mid)
        {
            delete algo;
            return solve_binary_search(algo_name, instance, mid + 1, r);
        }
        delete algo;
        return solve_binary_search(algo_name, instance, l, mid - 1);
    }
    return algo->solveInstance(r);
}

//function that runs ANM algorithms and returns string of running duration and solution
string run_anm_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + int(0.05*LB);
    
    string row = "";
    int sol;
    // Always take solution of FirstFit as upper bound input for ANM-Strict Binary Search
    AlgoFit2D* algoFF = createAlgo2D("2DFF", instance);
    int UB = algoFF->solveInstance(hint_bin);
    UB += int(UB*0.1);
    delete algoFF;

    //batch size for iterative node activation approach
    int batch_size = int(LB*0.05);
    if (batch_size < 2) batch_size = 2;

    for (const string & algo_name : list_algos)
    {
        //Create algo objects for each relaxed, strict and iterative node activation approaches of algorithm
        AlgoFit2D * algo_relaxed = createAlgo2D(algo_name, instance);
        //AlgoFit2D * algo_strict = createAlgo2D(algo_name, instance);
        AlgoFit2D * algo_iterative_node_activation = createAlgo2D(algo_name, instance);

        //run relaxed algorithm with current app node scoring system
        if (algo_relaxed != nullptr)
        {
            cout << "Running for " << algo_name << " Relaxed: ";
            auto start = high_resolution_clock::now();
            sol = algo_relaxed->solveInstance(hint_bin);
            auto stop = high_resolution_clock::now();
            cout << " Sol: " << sol << endl;
            auto duration = duration_cast<milliseconds>(stop - start);

            row.append("," + to_string((float)duration.count() / 1000));
            delete algo_relaxed;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
        
        //run binary search with current app node scoring system
            cout << "Running for " << algo_name << " Strict: " << endl;
            auto start = high_resolution_clock::now();
            sol = solve_binary_search(algo_name, instance, LB, UB);
            auto stop = high_resolution_clock::now();
            cout << "Lower bound: " << LB << "Upper bound: " << UB << "Sol: " << sol << endl;
            auto duration = duration_cast<milliseconds>(stop - start);

            row.append("," + to_string((float)duration.count() / 1000));
            
        //run iterative batch approach with current app node scoring system
        if (algo_iterative_node_activation != nullptr)
        {
            cout << "Running for " << algo_name << " Iterative" << endl;
            
            auto start = high_resolution_clock::now();
            sol = algo_iterative_node_activation->solveInstance(batch_size, 1);
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<milliseconds>(stop - start);

            row.append("," + to_string((float)duration.count() / 1000));
            delete algo_iterative_node_activation;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
    return row;
}


void tc_all_algos(string input_path, vector<string>& list_algos, vector<string>& list_algos_anm, vector<string>& list_spread_algos) {
    //default set to 64 CPU and 128 Memory
    int bin_cpu_capacity = 64;
    int bin_mem_capacity = 128;

    string outfile("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\output\\tc_all_LRAS_TEST.csv");

    //open file for writing and check it opens correctly
    ofstream f(outfile, ios_base::trunc);
    if (!f.is_open())
    {
        cout << "Cannot write file " << outfile << endl;
        return;
    }
    cout << "Writing output to file " << outfile << endl;

    string header("instance_name,aff_density");

    for (std::string algo_name : list_algos)
    {
        header.append("," + algo_name);
    }
    for (std::string algo_name_spread : list_spread_algos)
    {
        header.append("," + algo_name_spread);
    }
    for (std::string algo_name_anm : list_algos_anm)
    {
        header.append("," + algo_name_anm + "-Relaxed");
        header.append("," + algo_name_anm + "-Strict");
        header.append("," + algo_name_anm + "-IterativeNodeActivation");
    }

    //add header -> each anm scoring method.
    f << header << "\n";

    string lras[9] = {"10","25","50","75","100","150","200","250","500"};

   
    for(int i = 0; i < 9; ++i) {
            cout << "Starting running time analysis for: " << lras[i] << " size." << endl;
            string instance_name(lras[i] + "_");
            string infile(input_path + lras[i] + ".csv");

            const Instance2D instance(instance_name, bin_cpu_capacity, bin_mem_capacity, infile);

            string row_str = run_non_spread_algos(instance, list_algos);
            row_str += run_spread_algos(instance, list_spread_algos);
            row_str += run_anm_algos(instance, list_algos_anm);
            f << instance_name << "," << lras[i] << row_str <<"\n";
            f.flush();
    }

    f.close();

}



//This function is to test running time (time complexity) of App Node Matching Algos
void tc_anm_algos(string input_path, vector<string>& list_algos_anm) {
    //default set to 64 CPU and 128 Memory
    int bin_cpu_capacity = 64;
    int bin_mem_capacity = 128;

    string outfile("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\output\\tc_anm_affinities.csv");

    //open file for writing and check it opens correctly
    ofstream f(outfile, ios_base::trunc);
    if (!f.is_open())
    {
        cout << "Cannot write file " << outfile << endl;
        return;
    }
    cout << "Writing output to file " << outfile << endl;

    string header("instance_name,number_lras");

    for (std::string algo_name : list_algos_anm)
    {
        header.append("," + algo_name);
    }
    //add header -> each anm scoring method.
    f << header << "\n";

    string affinities[16] = {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};

    //Run anm algos on the CSV files defined and print results to file
    for(int i = 0; i < 16; ++i) {
        for(int j = 0; j < 3; j++) {
            cout << "Starting running time analysis for: " << affinities[i] << " affinity density." << endl;
            string instance_name("250_aff-" + affinities[i] + "_" + to_string(j));
            string infile(input_path + "250_aff-" + affinities[i] + "_" + to_string(j) + ".csv");

            const Instance2D instance(instance_name, bin_cpu_capacity, bin_mem_capacity, infile);

            string row_str = run_non_spread_algos(instance, list_algos_anm);
            f << instance_name << "," << affinities[i] << row_str <<"\n";
            f.flush();
        }
    }

    f.close();

}

int main(int argc, char** argv) {
    vector<string> list_all_algos_anm = {
        //App Node Matching Algorithms
        "AlgoAppNodeMatch2DDP", 
        "AlgoAppNodeMatch2DDynamicNormDP",
        "AlgoAppNodeMatch2DNormDP",
        "AlgoAppNodeMatch2DL2Norm",
        "AlgoAppNodeMatch2DTightFill"
    };

    vector<string> list_algos_anm = {
        //App Node Matching Algorithms
        // "AlgoAppNodeMatch2DDP", 
        // "AlgoAppNodeMatch2DDynamicNormDP",
        // "AlgoAppNodeMatch2DNormDP",
        "AlgoAppNodeMatch2DL2Norm",
        //"AlgoAppNodeMatch2DTightFill"
    };

    vector<string> list_all_algos = {
        //App Centric

        //CHOSEN
        "2DFF",
        // "2DFFDDegree",
        // "2DFFDAvg",

        //CHOSEN
        "2DFFDMax",

        // "2DFFDAvgExpo",
        // "2DFFDSurrogate",
        // "2DFFDExtendedSum",

        //CHOSEN
        "2DBFDAvg",

        // "2DBFDMax",
        // "2DBFDAvgExpo",
        // "2DBFDSurrogate",
        // "2DBFDExtendedSum",
        // "2DWFDAvg",
        // "2DWFDMax",
        // "2DWFDAvgExpo",
        // "2DWFDSurrogate",
        // "2DWFDExtendedSum",

        // "2DNodeCount",

        // //Node Centric
        // "2DFFDDotProduct",
        // "2DFFDL2Norm",

        //CHOSEN
        "2DFFDFitness",
    };

    vector<string> list_spread_algos = {
        "SpreadWFD-Avg",
        // "SpreadWFD-Max",
        // "SpreadWFD-AvgExpo",
        // "SpreadWFD-Surrogate",
        // "SpreadWFD-ExtendedSum",

        // "RefineWFD-Avg-5",
        // "RefineWFD-Avg-3",
        "RefineWFD-Avg-2"
    };

    string input_path("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\data\\time_complexity\\");

    //run sub experiment on anm algos first then on all algos
    tc_anm_algos(input_path, list_all_algos_anm);
    tc_all_algos(input_path, list_all_algos, list_algos_anm, list_spread_algos);
    return 0;
}