#include "application.hpp"
#include "instance.hpp"
#include "lower_bounds.hpp"
#include "..\\algos\\algos2D.hpp"

#include <iostream>
#include <fstream>
#include <chrono>
#include <map>

using namespace std;
using namespace std::chrono;

map<string, int> algo_results;
map<string, int> algo_app_node_match_results;

// THIS FILE WAS USED FOR TESTING, MAIN EXPERIMENT EXECUTABLES ARE:
// - placement_quality
// - resource_utilisation
// - time_complexity

//for testing purposes - iterate through algorithms and print the number of bins returned
void print_algo_results()
{
    for (auto it = algo_results.begin(); it != algo_results.end(); ++it)
    {
        cout << it->first << "\t\t" << it->second << endl;
    }
    for (auto it2 = algo_app_node_match_results.begin(); it2 != algo_app_node_match_results.end(); ++it2)
    {
        cout << it2->first << "\t\t" << it2->second << endl;
    }
}

//from app and node centric approaches, return the best performing algorithm
const string find_best_algo()
{
    string best_algo;
    int min = std::numeric_limits<int>::max();
    for (auto it = algo_results.begin(); it != algo_results.end(); ++it)
    {
        if (it->second < min)
        {
            best_algo = it->first;
        }
    }
    return best_algo;
}

//this has been inspired from clement mommessin's code
void initial_algo_list_run(string input_file, string output_file, int bin_cpu_capacity, int bin_mem_capacity, vector<string> &list_algos)
{
    const Instance2D instance("ID1", bin_cpu_capacity, bin_mem_capacity, input_file);
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + 200;

    int sol;
    for (const string &algo_name : list_algos)
    {
        AlgoFit2D *algo = createAlgo2D(algo_name, instance);
        if (algo != nullptr)
        {
            sol = algo->solveInstance(hint_bin);
            algo_results[algo_name] = sol;

            delete algo;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
}

int solve_binary_search(AlgoFit2D *algo, string algo_name, int l, int r)
{
    int curr_sol;
    int one_less_bin_sol;
    /*
    Using best current result as an upper bound and the LB as the lower bound, see if any amount of 
    preactivated nodes/bins results in a better approximate solution.
    Executed using a recursive approach.
    */
    if (r > l)
    {
        if (r == l)
        {
            return algo->solveInstance(r);
        }
        int mid = (l + (r - 1)) / 2;
        cout << "Solving: " << algo_name << " with bins: " << mid << endl;
        curr_sol = algo->solveInstance(mid);
        cout << "Solving (extra bin check): " << algo_name << " with bins: " << mid << endl;
        one_less_bin_sol = algo->solveInstance(mid - 1);
        cout << "Current Solution: " << curr_sol << "\tMid: " << mid << endl;
        if (curr_sol == mid && one_less_bin_sol > mid)
        {
            return mid;
        }
        if (curr_sol > mid)
        {
            return solve_binary_search(algo, algo_name, mid + 1, r);
        }
        return solve_binary_search(algo, algo_name, l, mid - 1);
    }
    return algo->solveInstance(r);
}

void run_app_node_binary_search(int best_result, string input_file, int bin_cpu_capacity, int bin_mem_capacity)
{
    const Instance2D instance("ID1", bin_cpu_capacity, bin_mem_capacity, input_file);
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);
    //list of app-node matching algorithms
    vector<string> list_app_node_match_algos = {
        "AlgoAppNodeMatch2DDP",
        "AlgoAppNodeMatch2DNormDP",
        "AlgoAppNodeMatch2DDynamicNormDP",
        "AlgoAppNodeMatch2DL2Norm"};
    
    /*for each app_node matching algo, see if a feasible solution (doesn't devolve to node centric) exists when
        the "best result" amount of nodes are activated
        because of monotonicity principle, if the result is feasible, it is either to optimal number of bins to 
        activate or this number lies within LB and best_result UB so therefore binary search method is used*/
    for (auto it = list_app_node_match_algos.begin(); it != list_app_node_match_algos.end(); ++it)
    {
        AlgoFit2D *algo = createAlgo2D(*it, instance);
        int check = algo->solveInstance(best_result);
        if (check > best_result)
        {
            algo_app_node_match_results[*it] = check;
            cout << *it << " cannot be solved with fewer bins, solved with: " << check << endl;
        }
        else
        {
            algo_app_node_match_results[*it] = solve_binary_search(algo, *it, LB, best_result);
        }
        delete algo;
    }
    return;
}

int main(int argc, char **argv)
{
    //input dataset and output file.
    string input_file("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\clement_datasets\\for_jack\\datasets\\dataset_250_0.csv");
    string output_file("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\output\\binary_search_test.csv");

    //Bin 2D restraints - this should become variable dependent on the dataset
    int bin_cpu_capacity = 64;
    int bin_mem_capacity = 64;

    //list of node centric + app centric algorithms
    vector<string> list_algos = {
        "2DFF", "2DFFDDegree",

        "2DFFDAvg", "2DFFDMax",
        "2DFFDAvgExpo", "2DFFDSurrogate",
        "2DFFDExtendedSum",

        "2DBFDAvg", "2DBFDMax",
        "2DBFDAvgExpo", "2DBFDSurrogate",
        "2DBFDExtendedSum",

        "2DFFDL2Norm", "2DFFDDotProduct", "2DFFDFitness"};
    
    initial_algo_list_run(input_file, output_file, bin_cpu_capacity, bin_mem_capacity, list_algos);
    //identify best performing node/app centric algo, and store the amount of bins it allocated
    const string best_algo = find_best_algo();
    int best_result = algo_results[best_algo];
    //test - todo remove
    print_algo_results();
    run_app_node_binary_search(best_result, input_file, bin_cpu_capacity, bin_mem_capacity);
    //test - todo remove
    print_algo_results();
    return 0;
}