#include "application.hpp"
#include "instance.hpp"
#include "lower_bounds.hpp"
#include "..\\algos\\algos2D.hpp"

#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;
using namespace std::chrono;

//function to run non spreading algorithms and return string of running duration and solution
string run_non_spread_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + int(0.05*LB);

    int max_bins = int(LB*0.9);
    
    string row("," + to_string(max_bins));
    int sol;
    for (const string & algo_name : list_algos)
    {
        //solve instance with each algo and calculate running time
        AlgoFit2D * algo = createAlgo2D(algo_name, instance);
        if (algo != nullptr)
        {
            cout << "Running for " << algo_name << endl;
            auto start = high_resolution_clock::now();
            sol = algo->solveInstance(hint_bin, 0, max_bins);
            auto stop = high_resolution_clock::now();
            auto remaining_replicas = algo->getFinalReplicas();
            auto remaining_cpu = algo->getFinalCPUDemand();
            auto remaining_mem = algo->getFinalMemDemand();
            auto final_bins = algo->getFinalBins();
            auto duration = duration_cast<milliseconds>(stop - start);

            row.append("," + to_string(remaining_replicas) + "," + to_string(remaining_cpu) + "," + to_string(remaining_mem) + "," + to_string(final_bins) + "," + to_string((float)duration.count() / 1000));
            delete algo;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
    return row;
}

//function that runs spread algorithms and return string of running duration and solution
string run_spread_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);
    
    int max_bins = int(LB*0.9);

    string row = "";
    int sol;

    int hint_bin = LB + int(0.05*LB);

    // Always take solution of FirstFit as upper bound input
    AlgoFit2D* algoFF = createAlgo2D("2DFF", instance);
    int UB = algoFF->solveInstance(hint_bin);
    delete algoFF;

    //Calculate spread algo running time
    for (const string & algo_name : list_algos)
    {
        Algo2DSpreadWFDAvg * algo = createSpreadAlgo(algo_name, instance);
        if (algo != nullptr)
        {
            cout << "Running for " << algo_name << endl;
            auto start = high_resolution_clock::now();
            bool rtn = algo->trySolve(max_bins);
            auto remaining_replicas = algo->getFinalReplicas();
            auto remaining_cpu = algo->getFinalCPUDemand();
            auto remaining_mem = algo->getFinalMemDemand();
            auto final_bins = algo->getFinalBins();
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<seconds>(stop - start);

            row.append("," + to_string(remaining_replicas) + "," + to_string(remaining_cpu) + "," + to_string(remaining_mem) + "," + to_string(final_bins) + "," + to_string((float)duration.count() / 1000));
            delete algo;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
    return row;
}

//function that runs ANM algorithms and returns string of running duration and solution
string run_anm_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + int(0.05*LB);

    int max_bins = int(LB*0.9);
    
    string row = "";
    int sol;
    // Always take solution of FirstFit as upper bound input for ANM-Strict Binary Search
    AlgoFit2D* algoFF = createAlgo2D("2DFF", instance);
    int UB = algoFF->solveInstance(hint_bin);
    UB += int(UB*0.1);
    delete algoFF;

    //batch size for iterative node activation approach
    int batch_size = int(LB*0.1);
    if (batch_size < 2) batch_size = 2;

    for (const string & algo_name : list_algos)
    {
        //Create algo objects for each relaxed, strict and iterative node activation approaches of algorithm
        AlgoFit2D * algo_strict = createAlgo2D(algo_name, instance);
        AlgoFit2D * algo_iterative_node_activation = createAlgo2D(algo_name, instance);

        //run strict algorithm with current app node scoring system
        if (algo_strict != nullptr)
        {
            cout << "Running for " << algo_name << " Strict" << endl;
            auto start = high_resolution_clock::now();
            sol = algo_strict->solveInstance(max_bins, 0 , max_bins);
            auto stop = high_resolution_clock::now();
            auto remaining_replicas = algo_strict->getFinalReplicas();
            auto remaining_cpu = algo_strict->getFinalCPUDemand();
            auto remaining_mem = algo_strict->getFinalMemDemand();
            auto final_bins = algo_strict->getFinalBins();
            auto duration = duration_cast<milliseconds>(stop - start);

            row.append("," + to_string(remaining_replicas) + "," + to_string(remaining_cpu) + "," + to_string(remaining_mem) + "," + to_string(final_bins) + "," + to_string((float)duration.count() / 1000));
            delete algo_strict;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }

        //run iterative batch approach with current app node scoring system
        if (algo_iterative_node_activation != nullptr)
        {
            cout << "Running for " << algo_name << " Iterative" << endl;
            
            auto start = high_resolution_clock::now();
            sol = algo_iterative_node_activation->solveInstance(batch_size, 1, max_bins);
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<milliseconds>(stop - start);
            auto remaining_replicas = algo_iterative_node_activation->getFinalReplicas();
            auto remaining_cpu = algo_iterative_node_activation->getFinalCPUDemand();
            auto remaining_mem = algo_iterative_node_activation->getFinalMemDemand();
            auto final_bins = algo_iterative_node_activation->getFinalBins();
            row.append("," + to_string(remaining_replicas) + "," + to_string(remaining_cpu) + "," + to_string(remaining_mem) + "," + to_string(final_bins) + "," + to_string((float)duration.count() / 1000));
            delete algo_iterative_node_activation;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
    return row;
}


void pq_all_algos(string input_path, vector<string>& list_algos, vector<string>& list_algos_anm, vector<string>& list_spread_algos) {
    //default set to 64 CPU and 128 Memory
    int bin_cpu_capacity = 64;
    int bin_mem_capacity = 128;

    
    //change as necessary
    string outfile("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\output\\pq_all_new_PROPER.csv");

    //open file for writing and check it opens correctly
    ofstream f(outfile, ios_base::trunc);
    if (!f.is_open())
    {
        cout << "Cannot write file " << outfile << endl;
        return;
    }
    cout << "Writing output to file " << outfile << endl;

    string header("instance_name,number_lras,affinity,file_num,num_replicas,total_cpu,total_mem,max_bins");

    //write header of CSV output file
    for (std::string algo_name : list_algos)
    {
        header.append("," + algo_name + "_replicas_left");
        header.append("," + algo_name + "_cpu_left");
        header.append("," + algo_name + "_mem_left");
        header.append("," + algo_name + "_bins_used");
        header.append("," + algo_name + "_time");
    }
    for (std::string algo_name_spread : list_spread_algos)
    {
        header.append("," + algo_name_spread + "_replicas_left");
        header.append("," + algo_name_spread + "_cpu_left");
        header.append("," + algo_name_spread + "_mem_left");
        header.append("," + algo_name_spread + "_bins_used");
        header.append("," + algo_name_spread + "_time");
    }
    for (std::string algo_name_anm : list_algos_anm)
    {
        header.append("," + algo_name_anm + "-Strict_replicas_left");
        header.append("," + algo_name_anm + "-Strict_cpu_left");
        header.append("," + algo_name_anm + "-Strict_mem_left");
        header.append("," + algo_name_anm + "-Strict_bins_used");
        header.append("," + algo_name_anm + "-Strict_time");
        header.append("," + algo_name_anm + "-IterativeNodeActivation_replicas_left");
        header.append("," + algo_name_anm + "-IterativeNodeActivation_cpu_left");
        header.append("," + algo_name_anm + "-IterativeNodeActivation_mem_left");
        header.append("," + algo_name_anm + "-IterativeNodeActivation_bins_used");
        header.append("," + algo_name_anm + "-IterativeNodeActivation_time");
    }

    //add header -> each anm scoring method.
    f << header << "\n";

    // string lra_nums[12] = {"10","25","50","100","250","500","1000","2000","3000","4000","5000","10000"};

    // //testing
    // for(int i = 0; i < 5; ++i) {
    //     cout << "Starting running time analysis for: " << lra_nums[i] << " LRA size." << endl;
    //     string instance_name("LRAs_" + lra_nums[i]);
    //     string infile(input_path + lra_nums[i] + ".csv");

    //     const Instance2D instance(instance_name, bin_cpu_capacity, bin_mem_capacity, infile);

    //     string row_str = run_non_spread_algos(instance, list_algos);
    //     row_str += run_spread_algos(instance, list_spread_algos);
    //     row_str += run_anm_algos(instance, list_algos_anm);
    //     f << instance_name << "," << lra_nums[i] << row_str <<"\n";
    //     f.flush();
    // }


    //change as required to reflect CSV input instances to execute on
    vector<string> lra_nums = {
        "100",
        "250",
        "500"
        //"1000"
    };

    vector<string> aff_densities = {
        "aff-0",
        "aff-1",
        "aff-5",
        "aff-10",
        "aff-intra"
    };


    //Run all algos on the CSV files defined and print results to file
    for (string lra_num : lra_nums)
    {
        for (string aff_density : aff_densities)
        {
            for (int i = 1; i < 4; ++i) 
            {
                string instance_name("" + lra_num + "_" + aff_density + "_" + to_string(i));
                string row_str("" + instance_name + "," + lra_num + "," + aff_density + "," + to_string(i));
                string infile(input_path + instance_name + ".csv");

                const Instance2D instance(instance_name, bin_cpu_capacity, bin_mem_capacity, infile);

                cout << "Running for instance: " << instance_name << endl;

                row_str += string("," + to_string(instance.getTotalReplicas()) + "," + to_string(instance.getSumCPU()) + "," + to_string(instance.getSumMem()));

                row_str += run_non_spread_algos(instance, list_algos);
                row_str += run_spread_algos(instance, list_spread_algos);
                row_str += run_anm_algos(instance, list_algos_anm);

                f << row_str << "\n";
                f.flush();
            }
        }
    }

    f.close();

}

int main(int argc, char** argv) {
    vector<string> list_algos_anm = {
        //App Node Matching Algorithms
        "AlgoAppNodeMatch2DDP", 
        "AlgoAppNodeMatch2DDynamicNormDP",
        "AlgoAppNodeMatch2DNormDP",
        "AlgoAppNodeMatch2DL2Norm",
        "AlgoAppNodeMatch2DTightFill"
    };

    //list of all algos, comment out ones that are / aren't applicable
    vector<string> list_all_algos = {
        //App Centric
        "2DFF",
        // "2DFFDDegree",
        // "2DFFDAvg",
        "2DFFDMax",
        // "2DFFDAvgExpo",
        // "2DFFDSurrogate",
        // "2DFFDExtendedSum",
        "2DBFDAvg",
        // "2DBFDMax",
        // "2DBFDAvgExpo",
        // "2DBFDSurrogate",
        // "2DBFDExtendedSum",
        // "2DWFDAvg",
        // "2DWFDMax",
        // "2DWFDAvgExpo",
        // "2DWFDSurrogate",
        // "2DWFDExtendedSum",

        // "2DNodeCount",

        // //Node Centric
        // "2DFFDDotProduct",
        // "2DFFDL2Norm",
        "2DFFDFitness",
    };

    vector<string> list_spread_algos = {
        "SpreadWFD-Avg",
        // "SpreadWFD-Max",
        // "SpreadWFD-AvgExpo",
        // "SpreadWFD-Surrogate",
        // "SpreadWFD-ExtendedSum",

    };

    //change where necessary
    string input_path("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\data\\");

    pq_all_algos(input_path, list_all_algos, list_algos_anm, list_spread_algos);
    return 0;
}