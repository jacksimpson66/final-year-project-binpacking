//This file was heavily copied from work of Clement Mommessin, with slight moderations

#include "application.hpp"
#include "instance.hpp"
#include "lower_bounds.hpp"
#include "..\\algos\\algos2D.hpp"

#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;
using namespace std::chrono;

// THIS FILE WAS USED FOR TESTING, MAIN EXPERIMENT EXECUTABLES ARE:
// - placement_quality
// - resource_utilisation
// - time_complexity

std::string run_for_instance(const Instance2D & instance, const vector<string> & list_algos)
{
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + 200;

    string row(to_string(LB_cpu) + "\t" + to_string(LB_mem));
    string row_time;

    int sol;
    cout << "Lower Bound = " << LB << endl;
    for (const string & algo_name : list_algos)
    {
        AlgoFit2D * algo = createAlgo2D(algo_name, instance);
        if (algo != nullptr) {
            if(algo_name == "AlgoAppNodeMatch2DDP" || algo_name == "AlgoAppNodeMatch2DNormDP" || algo_name == "AlgoAppNodeMatch2DDynamicNormDP" 
            || algo_name == "AlgoAppNodeMatch2DL2Norm" || algo_name == "AlgoAppNodeMatch2DTightFill") {
                for(int i = 5; i <= 25; i+=5) {
                    algo = createAlgo2D(algo_name, instance);
                    hint_bin = i;
                    cout << "Running batch for " << algo_name << " with batch size: " << hint_bin;
                    auto start = high_resolution_clock::now();
                    sol = algo->solveInstance(hint_bin, 1);
                    auto stop = high_resolution_clock::now();
                    auto duration = duration_cast<milliseconds>(stop - start);
                    cout << " Solution: " << sol << " Duration: ";
                    cout << (float)duration.count()/1000 << endl;

                    row.append("\t" + to_string(sol));
                    row_time.append("\t" + to_string((float)duration.count() / 1000));
                    delete algo;
                }
                
            }
            else
            {
                cout << "Running for " << algo_name;
                auto start = high_resolution_clock::now();
                sol = algo->solveInstance(hint_bin);
                auto stop = high_resolution_clock::now();
                auto duration = duration_cast<milliseconds>(stop - start);
                cout << " Solution: " << sol << " Duration: ";
                cout << (float)duration.count()/1000 << endl;

                row.append("\t" + to_string(sol));
                row_time.append("\t" + to_string((float)duration.count() / 1000));
                delete algo;
            }
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }

    row.append(row_time);
    return row;
}


int test_run_list_algos(string input_path, string& outfile, vector<string>& list_algos, int bin_cpu_capacity, int bin_mem_capacity)
{
    ofstream f(outfile, ios_base::trunc);
    if (!f.is_open())
    {
        cout << "Cannot write file " << outfile << endl;
        return -1;
    }
    cout << "Writing output to file " << outfile << endl;

    // Header line
    string header("instance_name\tLB_cpu\tLB_mem");
    string time_header;

    for (std::string algo_name : list_algos)
    {
        header.append("\t" + algo_name);
        time_header.append("\t" + algo_name + "_time");
    }
    f << header << time_header << "\n";

    // vector<int> densities = { 1};//, 5, 10 };
    // vector<string> graph_classes = { "arbitrary", "normal", "threshold" };

    // for (int d : densities)
    // {
    //     for (string& graph_class : graph_classes)
    //     {
    //         cout << "Starting density " << to_string(d) << " graph class: " << graph_class << endl;
    //         for (int n = 0; n < 10; ++n)
    //         {
    //             string instance_name(graph_class + "_d" + to_string(d) + "_" + to_string(n));
    //             string infile(input_path + instance_name + ".csv");
    //             const Instance2D instance(instance_name, bin_cpu_capacity, bin_mem_capacity, infile);

    //             string row_str = run_for_instance(instance, list_algos);
    //             f << instance_name << "\t" << row_str << "\n";
    //         }
    //         f.flush();
    //     }
    // }

    cout << "Starting execution of algos" << endl;
    string infile(input_path);
    const Instance2D instance("ID1", bin_cpu_capacity, bin_mem_capacity, infile);
    string row_str = run_for_instance(instance, list_algos);
    f << "Instance name" << "\t" << row_str << "\n";
    f.flush();

    f.close();
    return 0;
}

int main(int argc, char** argv) {
    //change where required!
    string input_path("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\clement_datasets\\for_jack\\datasets");
    input_path += "\\dataset_250_0.csv";

    string output_path("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\output");
    string outfile(output_path + "\\test.tsv");

    int bin_cpu_capacity;
    int bin_mem_capacity;
    //bin resource dimension values
    if (argc > 2)
    {
        bin_cpu_capacity = stoi(argv[1]);
        bin_mem_capacity = stoi(argv[2]);
    }
    else
    {
        // cout << "Usage: " << argv[0] << " <bin_cpu_capacity> <bin_mem_capacity>" << endl;
        // return -1;
        bin_cpu_capacity = 64;
        bin_mem_capacity = 64;
    }

    //list of algos to run
    vector<string> list_algos = {
        "2DFF", "2DFFDDegree",

        "2DFFDAvg", "2DFFDMax",
        "2DFFDAvgExpo", "2DFFDSurrogate",
        "2DFFDExtendedSum",

        "2DBFDAvg", "2DBFDMax",
        "2DBFDAvgExpo", "2DBFDSurrogate",
        "2DBFDExtendedSum",

        "2DFFDL2Norm", "2DFFDDotProduct", "2DFFDFitness",
        //"2DNodeCount",
        "AlgoAppNodeMatch2DDP", "AlgoAppNodeMatch2DDynamicNormDP",
        "AlgoAppNodeMatch2DNormDP", "AlgoAppNodeMatch2DL2Norm",
        "AlgoAppNodeMatch2DTightFill",
    };

    test_run_list_algos(input_path, outfile, list_algos, bin_cpu_capacity, bin_mem_capacity);
    
}