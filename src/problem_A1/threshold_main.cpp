#include "application.hpp"
#include "instance.hpp"
#include "lower_bounds.hpp"
#include "..\\algos\\algos2D.hpp"

#include <iostream>
#include <fstream>
#include <chrono>
#include <map>
#include <cmath>

using namespace std;
using namespace std::chrono;

map<string, int> algo_results;
map<string, int> algo_app_node_match_results;
float initial_threshold = 0.9;

//for testing purposes - iterate through algorithms and print the number of bins returned
void print_algo_results()
{
    for (auto it = algo_results.begin(); it != algo_results.end(); ++it)
    {
        cout << it->first << "\t\t" << it->second << endl;
    }
    for (auto it2 = algo_app_node_match_results.begin(); it2 != algo_app_node_match_results.end(); ++it2)
    {
        cout << it2->first << "\t\t" << it2->second << endl;
    }
}

//from app and node centric approaches, return the best performing algorithm
const string find_best_algo()
{
    string best_algo;
    int min = std::numeric_limits<int>::max();
    for (auto it = algo_results.begin(); it != algo_results.end(); ++it)
    {
        if (it->second < min)
        {
            best_algo = it->first;
        }
    }
    return best_algo;
}

//this has been inspired from clement mommessin's code
void initial_algo_list_run(string input_file, string output_file, int bin_cpu_capacity, int bin_mem_capacity, vector<string> &list_algos)
{
    const Instance2D instance("ID1", bin_cpu_capacity, bin_mem_capacity, input_file);
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + 200;

    int sol;
    for (const string &algo_name : list_algos)
    {
        AlgoFit2D *algo = createAlgo2D(algo_name, instance);
        if (algo != nullptr)
        {
            sol = algo->solveInstance(hint_bin);
            algo_results[algo_name] = sol;

            delete algo;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
}

void run_app_node_threshold(int best_result, string input_file, int bin_cpu_capacity, int bin_mem_capacity)
{
    const Instance2D instance("ID1", bin_cpu_capacity, bin_mem_capacity, input_file);
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);
    int bins_to_activate = floor((float)best_result * initial_threshold);
    //list of app-node matching algorithms
    vector<string> list_app_node_match_algos = {
        "AlgoAppNodeMatch2DDP",
        "AlgoAppNodeMatch2DNormDP",
        "AlgoAppNodeMatch2DDynamicNormDP",
        "AlgoAppNodeMatch2DL2Norm"};
    
    /*for each app_node matching algo, see if a feasible solution (doesn't devolve to node centric) exists when
        the "best result" amount of nodes are activated
        because of monotonicity principle, if the result is feasible, it is either to optimal number of bins to 
        activate or this number lies within LB and best_result UB so therefore binary search method is used*/
    for (auto it = list_app_node_match_algos.begin(); it != list_app_node_match_algos.end(); ++it)
    {
        AlgoFit2D *algo = createAlgo2D(*it, instance);
        cout << *it << " Running with bins: " << bins_to_activate << endl;
        int sol = algo->solveInstance(bins_to_activate);
        algo_app_node_match_results[*it] = sol;
        delete algo;
    }
    return;
}

int main(int argc, char **argv)
{
    //input dataset and output file.
    string input_file("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\clement_datasets\\for_jack\\datasets\\dataset_250_0.csv");
    string output_file("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\output\\binary_search_test.csv");

    //Bin 2D restraints - this should become variable dependent on the dataset
    int bin_cpu_capacity = 64;
    int bin_mem_capacity = 64;


    //list of node centric + app centric algorithms
    vector<string> list_algos = {
        "2DFF", "2DFFDDegree",

        "2DFFDAvg", "2DFFDMax",
        "2DFFDAvgExpo", "2DFFDSurrogate",
        "2DFFDExtendedSum",

        "2DBFDAvg", "2DBFDMax",
        "2DBFDAvgExpo", "2DBFDSurrogate",
        "2DBFDExtendedSum",

        "2DFFDL2Norm", "2DFFDDotProduct", "2DFFDFitness"
    };
    
    initial_algo_list_run(input_file, output_file, bin_cpu_capacity, bin_mem_capacity, list_algos);
    //identify best performing node/app centric algo, and store the amount of bins it allocated
    const string best_algo = find_best_algo();
    int best_result = algo_results[best_algo];
    cout << best_result << endl;
    //test - todo remove
    print_algo_results();
    run_app_node_threshold(best_result, input_file, bin_cpu_capacity, bin_mem_capacity);
    //test - todo remove
    print_algo_results();
    return 0;
}