cmake_minimum_required(VERSION 3.2)
project(A1_executables)

#where to put all executables (cmake binary dir - top level of build tree)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

# test file
add_executable(test_main test_main.cpp lower_bounds.hpp ../algos/algos2D.hpp lower_bounds.cpp ../algos/algos2D.cpp)
target_link_libraries(test_main PRIVATE Binpack_lib)

add_executable(binary_search_main binary_search_main.cpp lower_bounds.hpp ../algos/algos2D.hpp lower_bounds.cpp ../algos/algos2D.cpp)
target_link_libraries(binary_search_main PRIVATE Binpack_lib)

add_executable(threshold_main threshold_main.cpp lower_bounds.hpp ../algos/algos2D.hpp lower_bounds.cpp ../algos/algos2D.cpp)
target_link_libraries(threshold_main PRIVATE Binpack_lib)

add_executable(batch_approach batch_approach.cpp lower_bounds.hpp ../algos/algos2D.hpp lower_bounds.cpp ../algos/algos2D.cpp)
target_link_libraries(batch_approach PRIVATE Binpack_lib)

add_executable(time_complexity_analysis time_complexity_analysis.cpp lower_bounds.hpp ../algos/algos2D.hpp lower_bounds.cpp ../algos/algos2D.cpp)
target_link_libraries(time_complexity_analysis PRIVATE Binpack_lib)

add_executable(placement_quality placement_quality.cpp lower_bounds.hpp ../algos/algos2D.hpp lower_bounds.cpp ../algos/algos2D.cpp)
target_link_libraries(placement_quality PRIVATE Binpack_lib)

add_executable(resource_utilisation resource_utilisation.cpp lower_bounds.hpp ../algos/algos2D.hpp lower_bounds.cpp ../algos/algos2D.cpp)
target_link_libraries(resource_utilisation PRIVATE Binpack_lib)