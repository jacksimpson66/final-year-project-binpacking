#include "application.hpp"
#include "instance.hpp"
#include "lower_bounds.hpp"
#include "..\\algos\\algos2D.hpp"

#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;
using namespace std::chrono;

//function to run non spreading algorithms and return string of running duration and solution
string run_non_spread_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + int(0.05*LB);

    int max_bins = int(LB*0.9);
    
    string row("," + to_string(LB));
    int sol;
    for (const string & algo_name : list_algos)
    {
        //solve instance with each algo and calculate running time
        AlgoFit2D * algo = createAlgo2D(algo_name, instance);
        if (algo != nullptr)
        {
            cout << "Running for " << algo_name << endl;
            auto start = high_resolution_clock::now();
            sol = algo->solveInstance(hint_bin);
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<milliseconds>(stop - start);

            row.append("," + to_string(sol) + "," + to_string((float)duration.count() / 1000));
            delete algo;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
    return row;
}

//function that runs spread algorithms and return string of running duration and solution
string run_spread_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);
    
    int max_bins = int(LB*0.9);

    string row = "";
    int sol;

    int hint_bin = LB + int(0.05*LB);

    // Always take solution of FirstFit as upper bound input
    AlgoFit2D* algoFF = createAlgo2D("2DFF", instance);
    int UB = algoFF->solveInstance(hint_bin);
    delete algoFF;

    //run for spreading algo
    for (const string & algo_name : list_algos)
    {
        Algo2DSpreadWFDAvg * algo = createSpreadAlgo(algo_name, instance);
        if (algo != nullptr)
        {
            cout << "Running for " << algo_name << endl;
            auto start = high_resolution_clock::now();
            sol = algo->solveInstanceSpread(LB, UB);
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<seconds>(stop - start);

            row.append("," + to_string(sol) + "," + to_string((float)duration.count() / 1000));
            delete algo;
        }
        else
        {
            cout << "Unknown algo name: " << algo_name << endl;
        }
    }
    return row;
}

//function that finds optimal number of bins for ANM-Strict - binary search
int solve_binary_search(string algo_name, const Instance2D & instance, int l, int r)
{
    int curr_sol;
    int one_less_bin_sol;

    AlgoFit2D* algo = createAlgo2D(algo_name, instance);
    /*
    Using best current result as an upper bound and the LB as the lower bound, see if any amount of 
    preactivated nodes/bins results in a better approximate solution.
    Executed using a recursive approach.
    */
    if (r > l)
    {
        if (r == l)
        {
            return algo->solveInstance(r);
        }
        int mid = (l + (r - 1)) / 2;
        auto start = high_resolution_clock::now();
        curr_sol = algo->solveInstance(mid);
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(stop - start);
        one_less_bin_sol = algo->solveInstance(mid - 1);
        if (curr_sol == mid && one_less_bin_sol > mid)
        {
            delete algo;
            return mid;
        }
        if (curr_sol > mid)
        {
            delete algo;
            return solve_binary_search(algo_name, instance, mid + 1, r);
        }
        delete algo;
        return solve_binary_search(algo_name, instance, l, mid - 1);
    }
    return algo->solveInstance(r);
}

//function that runs ANM algorithms and returns string of running duration and solution
string run_anm_algos(const Instance2D & instance, const vector<string> & list_algos)
{
    //calculate lowerbound to set threshold to.
    int LB_cpu = BPP2D_LBcpu(instance);
    int LB_mem = BPP2D_LBmem(instance);
    int LB = std::max(LB_cpu, LB_mem);

    int hint_bin = LB + int(0.05*LB);

    int max_bins = int(LB*0.9);
    
    string row("," + to_string(LB));
    int sol;
    // Always take solution of FirstFit as upper bound input for ANM-Strict Binary Search
    AlgoFit2D* algoFF = createAlgo2D("2DFF", instance);
    int UB = algoFF->solveInstance(hint_bin);
    UB += int(UB*0.1);
    delete algoFF;

    //batch size for iterative node activation approach
    int batch_size = int(LB*0.05);
    if (batch_size < 2) batch_size = 2;

    for (const string & algo_name : list_algos)
    {
        //Create algo objects for each relaxed, strict and iterative node activation approaches of algorithm
        AlgoFit2D * algo_relaxed = createAlgo2D(algo_name, instance);
        AlgoFit2D * algo_strict = createAlgo2D(algo_name, instance);
        AlgoFit2D * algo_iterative_node_activation = createAlgo2D(algo_name, instance);

        //run relaxed algorithm with current app node scoring system
        // if (algo_relaxed != nullptr)
        // {
        //     cout << "Running for " << algo_name << " Relaxed with hint_bin: " << hint_bin;
        //     auto start = high_resolution_clock::now();
        //     sol = algo_relaxed->solveInstance(hint_bin);
        //     auto stop = high_resolution_clock::now();
        //     auto duration = duration_cast<milliseconds>(stop - start);

        //     cout << " Sol: " << sol << " Duration: " << to_string((float)duration.count() / 1000) << endl;

        //     row.append("," + to_string(sol) + "," + to_string((float)duration.count() / 1000));
        //     delete algo_relaxed;
        // }
        // else
        // {
        //     cout << "Unknown algo name: " << algo_name << endl;
        // }

        //run strict algorithm with current app node scoring system
            cout << "Running for " << algo_name << " Strict.";
            auto start = high_resolution_clock::now();
            sol = solve_binary_search(algo_name, instance, LB, UB);
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<milliseconds>(stop - start);

            cout << " Lower bound: " << LB << " Upper bound: " << UB << " Sol: " << sol << " Duration: " << to_string((float)duration.count() / 1000) << endl;

            row.append("," + to_string(sol) + "," + to_string((float)duration.count() / 1000));
        

        //run iterative batch approach with current app node scoring system
        // if (algo_iterative_node_activation != nullptr)
        // {
        //     cout << "Running for " << algo_name << " Iterative.";
            
        //     auto start = high_resolution_clock::now();
        //     sol = algo_iterative_node_activation->solveInstance(batch_size, 1);
        //     auto stop = high_resolution_clock::now();
        //     auto duration = duration_cast<milliseconds>(stop - start);

        //     cout << " Sol: " << sol << " Duration: " << to_string((float)duration.count() / 1000) << endl;

        //     row.append("," + to_string(sol) + "," + to_string((float)duration.count() / 1000));
        //     delete algo_iterative_node_activation;
        // }
        // else
        // {
        //     cout << "Unknown algo name: " << algo_name << endl;
        // }
    }
    return row;
}


void ru_all_algos(string input_path, vector<string>& list_algos, vector<string>& list_algos_anm, vector<string>& list_spread_algos) {
    //default set to 64 CPU and 128 Memory
    int bin_cpu_capacity = 64;
    int bin_mem_capacity = 128;

    string outfile("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\output\\ru_250aff`.csv");

    //open file for writing and check it opens correctly
    ofstream f(outfile, ios_base::trunc);
    if (!f.is_open())
    {
        cout << "Cannot write file " << outfile << endl;
        return;
    }
    cout << "Writing output to file " << outfile << endl;

    string header("instance_name,number_lras,affinity,file_num,num_replicas,total_cpu,total_mem,LB");

    // for (std::string algo_name : list_algos)
    // {
    //     header.append("," + algo_name + "_sol");
    //     header.append("," + algo_name + "_time");
    // }
    // for (std::string algo_name_spread : list_spread_algos)
    // {
    //     header.append("," + algo_name_spread + "_sol");
    //     header.append("," + algo_name_spread + "_time");
    // }
    for (std::string algo_name_anm : list_algos_anm)
    {
        header.append("," + algo_name_anm + "-Strict_sol");
        header.append("," + algo_name_anm + "-Strict_time");
    }

    //add header -> each anm scoring method.
    f << header << "\n";

    // string lra_nums[12] = {"10","25","50","100","250","500","1000","2000","3000","4000","5000","10000"};

    // //testing
    // for(int i = 0; i < 5; ++i) {
    //     cout << "Starting running time analysis for: " << lra_nums[i] << " LRA size." << endl;
    //     string instance_name("LRAs_" + lra_nums[i]);
    //     string infile(input_path + lra_nums[i] + ".csv");

    //     const Instance2D instance(instance_name, bin_cpu_capacity, bin_mem_capacity, infile);

    //     string row_str = run_non_spread_algos(instance, list_algos);
    //     row_str += run_spread_algos(instance, list_spread_algos);
    //     row_str += run_anm_algos(instance, list_algos_anm);
    //     f << instance_name << "," << lra_nums[i] << row_str <<"\n";
    //     f.flush();
    // }

    vector<string> lra_nums = {
        // "100",
        "250",
        //"500"
    };

    vector<string> aff_densities = {
        // "aff-0",
        "aff-1",
        // "aff-5",
        // "aff-10",
        //"aff-intra"
    };

    // for(int j = 2; j < 4; j++){
    //     string instance_name2("500_aff-1_" + to_string(j));
    //     string row_str("" + instance_name2 + "," + "500" + "," + "aff-1" + "," + to_string(j));
    //     string infile(input_path + instance_name2 + ".csv");

    //     const Instance2D instance2(instance_name2, bin_cpu_capacity, bin_mem_capacity, infile);

    //     cout << "LRA 500. aff_1: " << j << ". Number replicas: " << instance2.getTotalReplicas() << " Total CPU: " << instance2.getSumCPU() << " Total Mem: " << instance2.getSumMem() << endl;

    //     row_str += string("," + to_string(instance2.getTotalReplicas()) + "," + to_string(instance2.getSumCPU()) + "," + to_string(instance2.getSumMem()));

    //     //row_str += run_non_spread_algos(instance, list_algos);
    //     //row_str += run_spread_algos(instance, list_spread_algos);
    //     row_str += run_anm_algos(instance2, list_algos_anm);

    //     f << row_str << "\n";
    //     f.flush();
    // }

//Run all algos on the CSV files defined and print results to file
    for (string lra_num : lra_nums)
    {
        for (string aff_density : aff_densities)
        {
            for (int i = 1; i < 2; ++i) 
            {
                string instance_name("" + lra_num + "_" + aff_density + "_" + to_string(i));
                string row_str("" + instance_name + "," + lra_num + "," + aff_density + "," + to_string(i));
                string infile(input_path + instance_name + ".csv");

                const Instance2D instance(instance_name, bin_cpu_capacity, bin_mem_capacity, infile);

                cout << "LRA 500. aff: " << aff_density << " for " << i << ". Number replicas: " << instance.getTotalReplicas() << " Total CPU: " << instance.getSumCPU() << " Total Mem: " << instance.getSumMem() << endl;

                row_str += string("," + to_string(instance.getTotalReplicas()) + "," + to_string(instance.getSumCPU()) + "," + to_string(instance.getSumMem()));

                //row_str += run_non_spread_algos(instance, list_algos);
                row_str += run_spread_algos(instance, list_spread_algos);
                //row_str += run_anm_algos(instance, list_algos_anm);

                f << row_str << "\n";
                f.flush();
            }
        }
    }

    f.close();

}

int main(int argc, char** argv) {
    vector<string> list_algos_anm = {
        //App Node Matching Algorithms
        "AlgoAppNodeMatch2DDP", 
        "AlgoAppNodeMatch2DDynamicNormDP",
        "AlgoAppNodeMatch2DNormDP",
        "AlgoAppNodeMatch2DL2Norm",
        "AlgoAppNodeMatch2DTightFill"
    };

    vector<string> list_all_algos = {
        //App Centric
        "2DFF",
        // "2DFFDDegree",
        // "2DFFDAvg",
        "2DFFDMax",
        // "2DFFDAvgExpo",
        // "2DFFDSurrogate",
        // "2DFFDExtendedSum",
        "2DBFDAvg",
        // "2DBFDMax",
        // "2DBFDAvgExpo",
        // "2DBFDSurrogate",
        // "2DBFDExtendedSum",
        // "2DWFDAvg",
        // "2DWFDMax",
        // "2DWFDAvgExpo",
        // "2DWFDSurrogate",
        // "2DWFDExtendedSum",

        // "2DNodeCount",

        // //Node Centric
        // "2DFFDDotProduct",
        // "2DFFDL2Norm",
        "2DFFDFitness",
    };

    vector<string> list_spread_algos = {
        "SpreadWFD-Avg",
        // "SpreadWFD-Max",
        // "SpreadWFD-AvgExpo",
        // "SpreadWFD-Surrogate",
        // "SpreadWFD-ExtendedSum",

        // "RefineWFD-Avg-5",
        // "RefineWFD-Avg-3",
        "RefineWFD-Avg-2"

    };

    string input_path("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\data\\");

    ru_all_algos(input_path, list_all_algos, list_algos_anm, list_spread_algos);
    return 0;
}