#ifndef INSTANCE_HPP
#define INSTANCE_HPP

#include "application.hpp"

//COPIED FROM CLÉMENT MOMMESSIN ET AL

void my_trim(std::string& s);
AffinityMap constructAffinitiyMap(std::string& aff_str);


class Instance2D
{
public:
    Instance2D(std::string id, int bin_cpu_capacity, int bin_memory_capacity,
               std::string& filename);

    virtual ~Instance2D();

    const std::string& getId() const;
    const int getBinCPUCapacity() const;
    const int getBinMemCapacity() const;
    const AppList2D& getApps() const;
    //const float getLambda() const;

    const int getSumCPU() const;
    const int getSumMem() const;
    const int getTotalReplicas() const;

private:
    std::string id;       // The instance id
    int bin_cpu_capacity; // The bin capacity for cpu requirements
    int bin_mem_capacity; // The bin capacity for memory requirements
    AppList2D app_list; // The list of Application2D of this instance

    //float lambda;  // Total cpu required by all applications / (total cpu + total degree)

    int sum_mem;        // Total mem required by all replicas of apps
    int sum_cpu;        // Total cpu required by all replicas of apps
    int total_replicas;
};


#endif // INSTANCE_HPP
