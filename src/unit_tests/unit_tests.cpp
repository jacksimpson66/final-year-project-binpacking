#include "gtest/gtest.h"
#include "application.hpp"
#include "instance.hpp"
#include "..\\algos\\algos2D.hpp"
#include <iostream>
#include "unit_tests.hpp"

//iterate through bins used and return the total memory capacity of bins
int calculate_total_bin_mem_cap(AlgoFit2D *algo)
{
    BinList2D bins = algo->getBins();
    int total_bin_mem_cap = 0;
    for (auto it = bins.begin(); it != bins.end(); ++it)
    {
        total_bin_mem_cap += (*it)->getMaxMemCap();
    }
    std::cout << "total_bin_mem_cap: " << total_bin_mem_cap << std::endl;
    return total_bin_mem_cap;
}

//iterate through bins used and return total cpu capacity of bins
int calculate_total_bin_cpu_cap(AlgoFit2D *algo)
{
    BinList2D bins = algo->getBins();
    int total_bin_cpu_cap = 0;
    for (auto it = bins.begin(); it != bins.end(); ++it)
    {
        total_bin_cpu_cap += (*it)->getMaxCPUCap();
    }
    std::cout << "total_bin_cpu_cap: " << total_bin_cpu_cap << std::endl;
    return total_bin_cpu_cap;
}

//iterate through bins used and return  total amount of memory available 
int calculate_total_bin_mem_available(AlgoFit2D *algo)
{
    BinList2D bins = algo->getBins();
    int total_bin_mem_available = 0;
    for (auto it = bins.begin(); it != bins.end(); ++it)
    {
        total_bin_mem_available += (*it)->getAvailableMemCap();
    }
    std::cout << "total_bin_mem_av: " << total_bin_mem_available << std::endl;
    return total_bin_mem_available;
}

//iterate through bins used and return total amount of cpu available
int calculate_total_bin_cpu_available(AlgoFit2D *algo)
{
    BinList2D bins = algo->getBins();
    int total_bin_cpu_available = 0;
    for (auto it = bins.begin(); it != bins.end(); ++it)
    {
        total_bin_cpu_available += (*it)->getAvailableCPUCap();
    }
    std::cout << "total_bin_cpu_av: " << total_bin_cpu_available << std::endl;
    return total_bin_cpu_available;
}

//iterate through all replicas and return amount of memory used
int calculate_total_replica_mem_used(AlgoFit2D *algo)
{
    AppList2D apps = algo->getApps();
    int total_replica_mem_used = 0;
    for (auto it = apps.begin(); it != apps.end(); ++it)
    {
        int nb_replicas = (*it)->getNbReplicas();
        for (int i = 0; i < nb_replicas; ++i)
        {
            total_replica_mem_used += (*it)->getMemorySize();
        }
    }
    return total_replica_mem_used;
}

//iterate through all replicas and return the amount of cpu used
int calculate_total_replica_cpu_used(AlgoFit2D *algo)
{
    AppList2D apps = algo->getApps();
    int total_replica_cpu_used = 0;
    for (auto it = apps.begin(); it != apps.end(); ++it)
    {
        int nb_replicas = (*it)->getNbReplicas();
        for (int i = 0; i < nb_replicas; ++i)
        {
            total_replica_cpu_used += (*it)->getCPUSize();
        }
    }
    return total_replica_cpu_used;
}

//calculate the total number of replicas allocated
int calculate_total_replicas_allocated(AlgoFit2D *algo)
{
    AppList2D apps = algo->getApps();
    int total_replicas_allocated = 0;
    for (auto it = apps.begin(); it != apps.end(); ++it)
    {
        total_replicas_allocated += (*it)->getNbReplicas();
    }
    return total_replicas_allocated;
}

// int calculate_total_replica_mem_demand(const Instance2D instance) {
//     AppList2D* instance_apps = &instance.getApps();
//     int total_replica_mem_demand = 0;
//     for (auto it = instance_apps.begin(); it != instance_apps.end(); ++it)
//     {
//         int nb_replicas = (*it)->getNbReplicas();
//         for (int i = 0; i < nb_replicas; ++i)
//         {
//             total_replica_mem_demand += (*it)->getMemorySize();
//         }
//     }
//     return total_replica_mem_demand;
// }

// int calculate_total_replica_cpu_demand(const Instance2D instance) {
//     AppList2D* instance_apps = &instance.getApps();
//     int total_replica_cpu_demand = 0;
//     for (auto it = instance_apps.begin(); it != instance_apps.end(); ++it)
//     {
//         int nb_replicas = (*it)->getNbReplicas();
//         for (int i = 0; i < nb_replicas; ++i)
//         {
//             total_replica_cpu_demand += (*it)->getCPUSize();
//         }
//     }
//     return total_replica_cpu_demand;
// }

AlgoRun::AlgoRun(std::string input_file, std::string algo_name, int bin_cpu_capacity, int bin_mem_capacity, int nb_bins)
{
    const Instance2D instance("1", bin_cpu_capacity, bin_mem_capacity, input_file);
    AlgoFit2D *algo = createAlgo2D(algo_name, instance);
    sol = algo->solveInstance(nb_bins);
    total_bin_mem_cap = calculate_total_bin_mem_cap(algo);
    total_bin_cpu_cap = calculate_total_bin_cpu_cap(algo);
    total_bin_mem_used = total_bin_mem_cap - calculate_total_bin_mem_available(algo);
    total_bin_cpu_used = total_bin_cpu_cap - calculate_total_bin_cpu_available(algo);
    total_replica_mem_used = calculate_total_replica_mem_used(algo);
    total_replica_cpu_used = calculate_total_replica_cpu_used(algo);
    total_replica_mem_demand = instance.getSumMem();
    total_replica_cpu_demand = instance.getSumCPU();
    total_replicas_allocated = calculate_total_replicas_allocated(algo);
    total_replicas_instance = instance.getTotalReplicas();
    appsAllocated = &algo->getApps();
}

int AlgoRun::get_total_bin_mem_cap()
{
    return total_bin_mem_cap;
}
int AlgoRun::get_total_bin_cpu_cap()
{
    return total_bin_cpu_cap;
}
int AlgoRun::get_total_bin_mem_used()
{
    return total_bin_mem_used;
}
int AlgoRun::get_total_bin_cpu_used()
{
    return total_bin_cpu_used;
}
int AlgoRun::get_total_replica_mem_used()
{
    return total_replica_mem_used;
}
int AlgoRun::get_total_replica_cpu_used()
{
    return total_replica_cpu_used;
}
int AlgoRun::get_total_replica_mem_demand()
{
    return total_replica_mem_demand;
}
int AlgoRun::get_total_replica_cpu_demand()
{
    return total_replica_cpu_demand;
}
const AppList2D *AlgoRun::get_apps_allocated()
{
    return appsAllocated;
}
int AlgoRun::get_total_replicas_instance()
{
    return total_replicas_instance;
}
int AlgoRun::get_total_replicas_allocated()
{
    return total_replicas_allocated;
}


class AlgoRunTest : public testing::TestWithParam<std::string>
{
public:
    void SetUp() override
    {
        std::string algo_name = GetParam();
        std::string input_file("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\clement_datasets\\for_jack\\datasets\\test_dataset.csv");
        algo_r = new AlgoRun(input_file, algo_name, 64, 64, 5);
    }
    void TearDown() override
    {
        delete algo_r;
    }

protected:
    AlgoRun *algo_r;
};

//Test that bins are initialised with memory/cpu values, that bins are used and that
//replicas use memory and cpu
TEST_P(AlgoRunTest, BinAppValuesGreaterZeroTest)
{
    EXPECT_GT(algo_r->get_total_bin_mem_cap(), 0);
    EXPECT_GT(algo_r->get_total_bin_cpu_cap(), 0);
    EXPECT_GT(algo_r->get_total_bin_mem_used(), 0);
    EXPECT_GT(algo_r->get_total_bin_cpu_used(), 0);
    EXPECT_GT(algo_r->get_total_replica_mem_used(), 0);
    EXPECT_GT(algo_r->get_total_replica_cpu_used(), 0);
}

//Test that total replica usage of cpu and memory is the same as what bin has used (no longer available)
TEST_P(AlgoRunTest, BinUsageEqualReplicaDemandTest)
{
    EXPECT_EQ(algo_r->get_total_replica_cpu_used(), algo_r->get_total_bin_cpu_used());
    EXPECT_EQ(algo_r->get_total_replica_mem_used(), algo_r->get_total_bin_mem_used());
}

//Test that all replicas are allocated (number of replica demand vs number replicas allocated)
TEST_P(AlgoRunTest, CorrectNumberReplicasAllocatedTest)
{
    EXPECT_EQ(algo_r->get_total_replicas_instance(), algo_r->get_total_replicas_allocated());
}

//Test that all replica cpu and mem demand has been allocated
TEST_P(AlgoRunTest, AllReplicaMemAndCPUAllocated) {
    EXPECT_EQ(algo_r->get_total_replica_mem_demand(), algo_r->get_total_replica_mem_used());
    EXPECT_EQ(algo_r->get_total_replica_cpu_demand(), algo_r->get_total_replica_cpu_used());
}

//Test that replica cpu and mem allocation is less than bin capacities in these dimensions
TEST_P(AlgoRunTest, ReplicaUsageLessThanBinCapacity) {
    EXPECT_LE(algo_r->get_total_replica_mem_used(), algo_r->get_total_bin_mem_cap());
    EXPECT_LE(algo_r->get_total_replica_cpu_used(), algo_r->get_total_bin_cpu_cap());
}

//Initialise parameterised test suite with algorithm names
INSTANTIATE_TEST_SUITE_P(TestAppNodeMatchInst, AlgoRunTest,
                         testing::Values("AlgoAppNodeMatch2DDP", "AlgoAppNodeMatch2DDynamicNormDP",
                                         "AlgoAppNodeMatch2DNormDP", "AlgoAppNodeMatch2DL2Norm", "AlgoAppNodeMatch2DTightFill"));

AlgoRunBatch::AlgoRunBatch(std::string input_file, std::string algo_name, int bin_cpu_capacity, int bin_mem_capacity, int nb_bins)
{
    const Instance2D instance("1", bin_cpu_capacity, bin_mem_capacity, input_file);
    AlgoFit2D *algo = createAlgo2D(algo_name, instance);
    sol = algo->solveInstance(nb_bins,1);
    total_bin_mem_cap = calculate_total_bin_mem_cap(algo);
    total_bin_cpu_cap = calculate_total_bin_cpu_cap(algo);
    total_bin_mem_used = total_bin_mem_cap - calculate_total_bin_mem_available(algo);
    total_bin_cpu_used = total_bin_cpu_cap - calculate_total_bin_cpu_available(algo);
    total_replica_mem_used = calculate_total_replica_mem_used(algo);
    total_replica_cpu_used = calculate_total_replica_cpu_used(algo);
    total_replica_mem_demand = instance.getSumMem();
    total_replica_cpu_demand = instance.getSumCPU();
    total_replicas_allocated = calculate_total_replicas_allocated(algo);
    total_replicas_instance = instance.getTotalReplicas();
    appsAllocated = &algo->getApps();
}

int AlgoRunBatch::get_total_bin_mem_cap()
{
    return total_bin_mem_cap;
}
int AlgoRunBatch::get_total_bin_cpu_cap()
{
    return total_bin_cpu_cap;
}
int AlgoRunBatch::get_total_bin_mem_used()
{
    return total_bin_mem_used;
}
int AlgoRunBatch::get_total_bin_cpu_used()
{
    return total_bin_cpu_used;
}
int AlgoRunBatch::get_total_replica_mem_used()
{
    return total_replica_mem_used;
}
int AlgoRunBatch::get_total_replica_cpu_used()
{
    return total_replica_cpu_used;
}
int AlgoRunBatch::get_total_replica_mem_demand()
{
    return total_replica_mem_demand;
}
int AlgoRunBatch::get_total_replica_cpu_demand()
{
    return total_replica_cpu_demand;
}
const AppList2D *AlgoRunBatch::get_apps_allocated()
{
    return appsAllocated;
}
int AlgoRunBatch::get_total_replicas_instance()
{
    return total_replicas_instance;
}
int AlgoRunBatch::get_total_replicas_allocated()
{
    return total_replicas_allocated;
}


class AlgoRunBatchTest : public testing::TestWithParam<std::string>
{
public:
    void SetUp() override
    {
        std::string algo_name = GetParam();
        std::string input_file("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\clement_datasets\\for_jack\\datasets\\test_dataset.csv");
        algo_r = new AlgoRun(input_file, algo_name, 64, 64, 5);
    }
    void TearDown() override
    {
        delete algo_r;
    }

protected:
    AlgoRun *algo_r;
};

//Test that bins are initialised with memory/cpu values, that bins are used and that
//replicas use memory and cpu
TEST_P(AlgoRunBatchTest, BinAppValuesGreaterZeroTest)
{
    EXPECT_GT(algo_r->get_total_bin_mem_cap(), 0);
    EXPECT_GT(algo_r->get_total_bin_cpu_cap(), 0);
    EXPECT_GT(algo_r->get_total_bin_mem_used(), 0);
    EXPECT_GT(algo_r->get_total_bin_cpu_used(), 0);
    EXPECT_GT(algo_r->get_total_replica_mem_used(), 0);
    EXPECT_GT(algo_r->get_total_replica_cpu_used(), 0);
}

//Test that all replicas are allocated (number of replica demand vs number replicas allocated)
TEST_P(AlgoRunBatchTest, CorrectNumberReplicasAllocatedTest)
{
    EXPECT_EQ(algo_r->get_total_replicas_instance(), algo_r->get_total_replicas_allocated());
}

//Test that all replica cpu and mem demand has been allocated
TEST_P(AlgoRunBatchTest, AllReplicaMemAndCPUAllocated) {
    EXPECT_EQ(algo_r->get_total_replica_mem_demand(), algo_r->get_total_replica_mem_used());
    EXPECT_EQ(algo_r->get_total_replica_cpu_demand(), algo_r->get_total_replica_cpu_used());
}

//Test that replica cpu and mem allocation is less than bin capacities in these dimensions
TEST_P(AlgoRunBatchTest, ReplicaUsageLessThanBinCapacity) {
    EXPECT_LE(algo_r->get_total_replica_mem_used(), algo_r->get_total_bin_mem_cap());
    EXPECT_LE(algo_r->get_total_replica_cpu_used(), algo_r->get_total_bin_cpu_cap());
}

//Initialise parameterised test suite with algorithm names
INSTANTIATE_TEST_SUITE_P(TestAppNodeMatchInstBatch, AlgoRunBatchTest,
                         testing::Values("AlgoAppNodeMatch2DDP"/*, "AlgoAppNodeMatch2DDynamicNormDP",
                                         "AlgoAppNodeMatch2DNormDP", "AlgoAppNodeMatch2DL2Norm", "AlgoAppNodeMatch2DTightFill"*/));
