#include "gtest/gtest.h"
#include "application.hpp"
#include "instance.hpp"
#include "..\\algos\\algos2D.hpp"

int calculate_total_bin_mem_cap(AlgoFit2D* algo);

int calculate_total_bin_cpu_cap(AlgoFit2D* algo);

int calculate_total_bin_mem_available(AlgoFit2D* algo);

int calculate_total_bin_cpu_available(AlgoFit2D* algo);

int calculate_total_replica_mem_used(AlgoFit2D* algo);

int calculate_total_replica_cpu_used(AlgoFit2D* algo);

int calculate_total_replicas_allocated(AlgoFit2D* algo);

// int calculate_total_replica_mem_demand(const Instance2D instance);

// int calculate_total_replica_cpu_demand(const Instance2D instance);

class AlgoRun {
    public:
        AlgoRun(std::string input_file, std::string algo_name, int bin_cpu_capacity, int bin_mem_capacity, int nb_bins);
        int get_total_bin_mem_cap();
        int get_total_bin_cpu_cap();
        int get_total_bin_mem_used();
        int get_total_bin_cpu_used();
        int get_total_replica_mem_used();
        int get_total_replica_cpu_used();
        int get_total_replica_mem_demand();
        int get_total_replica_cpu_demand();
        const AppList2D* get_apps_allocated();
        int get_total_replicas_instance();
        int get_total_replicas_allocated();
    private:
        int total_bin_mem_cap;
        int total_bin_cpu_cap;
        int total_bin_mem_used;
        int total_bin_cpu_used;
        int total_replica_mem_used;
        int total_replica_cpu_used;
        int total_replica_mem_demand;
        int total_replica_cpu_demand;
        int sol;
        int total_replicas_instance;
        int total_replicas_allocated;
        const AppList2D* appsAllocated;
};

class AlgoRunBatch {
    public:
        AlgoRunBatch(std::string input_file, std::string algo_name, int bin_cpu_capacity, int bin_mem_capacity, int nb_bins);
        int get_total_bin_mem_cap();
        int get_total_bin_cpu_cap();
        int get_total_bin_mem_used();
        int get_total_bin_cpu_used();
        int get_total_replica_mem_used();
        int get_total_replica_cpu_used();
        int get_total_replica_mem_demand();
        int get_total_replica_cpu_demand();
        const AppList2D* get_apps_allocated();
        int get_total_replicas_instance();
        int get_total_replicas_allocated();
    private:
        int total_bin_mem_cap;
        int total_bin_cpu_cap;
        int total_bin_mem_used;
        int total_bin_cpu_used;
        int total_replica_mem_used;
        int total_replica_cpu_used;
        int total_replica_mem_demand;
        int total_replica_cpu_demand;
        int sol;
        int total_replicas_instance;
        int total_replicas_allocated;
        const AppList2D* appsAllocated;
};