from random import randint, choices
from secrets import choice
from distribution_calc import calc_distribution_stats
import csv
import numpy as np


#global variables -> value distributions are calculated from alibaba data centre trace
distribution_stats = calc_distribution_stats()

nb_replicas_values = distribution_stats[0]
nb_replicas_probabilities = distribution_stats[1]
cores_values = distribution_stats[2]
cores_probabilities = distribution_stats[3]
memory_values = distribution_stats[4]
memory_probabilities = distribution_stats[5]

#open file to write to and add processed variable data
def create_file(name, instance):
    file_path = './data/time_complexity/' + name + '.csv'
    with open(file_path, 'w', newline='') as out_file:
        tsv_writer = csv.writer(out_file, delimiter='\t')
        tsv_writer.writerow(['app_id', 'nb_instances', 'core', 'memory', 'inter_degree', 'inter_aff'])
        for app in instance:
            row_to_append = []
            row_to_append.append(str(app))
            row_to_append.append(str(instance[app]['nb_instances']))
            row_to_append.append(str(instance[app]['core']))
            row_to_append.append(str(instance[app]['memory']))
            row_to_append.append(str(instance[app]['inter_degree']))
            row_to_append.append(str(instance[app]['inter_aff']))
            tsv_writer.writerow(row_to_append)

def create_instance(name, num_apps, aff_level, inter_app_affinity=True):


    # empty dict to hold the instance
    instance = {}
    #used to check which LRAs are already constrained
    affinity_map = {}
    for i in range(1, num_apps+1):
        # for each LRA, create a dict to store its requirements and constraints
        instance[i] = {}
        # number of replicas, memory and cpu demand randomised
        cpu_demand = choices(cores_values, cores_probabilities)[0]
        
        mem_demand = choices(memory_values, memory_probabilities)[0]
        
        nb_instances = choices(nb_replicas_values, nb_replicas_probabilities)[0]
        # set cores, memory and nb_instances values of LRA dict
        instance[i]["core"] = cpu_demand
        instance[i]["memory"] = mem_demand
        instance[i]['nb_instances'] = nb_instances
        # initialise inter_degree to 0 and affinity constraint list
        instance[i]["inter_degree"] = 0
        instance[i]["inter_aff"] = []
        affinity_map[i] = []
    #handles when inter application affinity constraints are allowed
    if inter_app_affinity:
        # total amount of affinities allowed
        total_density = int((num_apps*aff_level) * num_apps)
        count_density = 0
        # whilst affinity density has not been breached, pick a random LRA and set affinity constraints with another LRA
        while count_density < total_density:
            lra_id_current = randint(1, num_apps)
            lra_id_affinity = randint(1, num_apps)
            #ensure that LRA has unique affinity constraints
            if lra_id_affinity in affinity_map[lra_id_current]:
                continue
            else:
                #if the LRA has been chosen to have affinities with itself, let at least 1 replica get assigned
                if lra_id_affinity == lra_id_current:
                    affinity_constraint = randint(1, 10)
                else:
                    affinity_constraint = randint(0, 10)
                instance[lra_id_current]["inter_aff"].append((lra_id_affinity, affinity_constraint))
                #add LRA_id_affinity to the current LRAs store of affinities they have
                affinity_map[lra_id_current].append(lra_id_affinity)
                instance[lra_id_current]["inter_degree"] += 1
                count_density += 1
    # handles specific case where only intra-application affinities are allowed
    else:
        #total amount of apps allowed to have intra application affinities, set to 10%
        total_density = int(num_apps * 0.20)
        count_density = 0
        # whilst affinity density has not been breached, pick a random LRA and set affinity constraints with itself
        while count_density < total_density:
            lra_id_current = randint(1, num_apps)
            affinity_constraint = randint(1, 10)
            instance[lra_id_current]["inter_aff"].append(
                (lra_id_current, affinity_constraint))
            instance[lra_id_current]["inter_degree"] += 1
            count_density += 1
    create_file(name, instance)


#change these variables to dictate LRA sizes and affinity densities for files created
lra_numbers = [100, 250, 500]
affinity_densities = [0, 1, "intra", 5, 10]


for i in range(1,6):
    for lra_num in lra_numbers:
        for aff in affinity_densities:
            if aff != 'intra':
                aff_name = int(aff*100)
                instance_name = '' + str(lra_num) + '_aff-' + str(aff_name) + '_' + str(i)
            else:
                instance_name = '' + str(lra_num) + '_aff-' + str(aff) + '_' + str(i)
            if aff == 'intra':
                create_instance(instance_name, lra_num, aff, False)
            else:
                create_instance(instance_name, lra_num, aff)

# for lra_num in lra_numbers:
#     instance_name = '' + str(lra_num)
#     create_instance(instance_name, lra_num, 0.01)


# for i in range(0,3):
#     for lra_num in lra_numbers:
#         for aff in affinity_densities:
#             aff_name = int(aff*100)
#             instance_name = '' + str(lra_num) + '_aff-' + str(aff_name) + "_" + str(i)
#             create_instance(instance_name, lra_num, aff)