import csv
import numpy as np
import matplotlib.pyplot as plt
'''
This python file calculates the normal distribution stats of the alibaba 2D dataset
'''

def calc_distribution_stats():
    #list to store alibaba dataset values for replicas, cpu demand and memory demand for LRA instances
    nb_replicas = []
    cores = []
    memory = []
    counter = 0

    #read in csv file and store data in lists
    with open('C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\alibaba_data\\TClab\\TClab_dataset_2D.csv') as in_file:
        read_tsv = csv.reader(in_file, delimiter='\t')
        header = next(read_tsv)
        for row in read_tsv:
            counter += 1
            nb_replicas.append(int(row[1]))
            cores.append(int(row[2]))
            memory.append(int(row[3]))
    

    nb_replicas_probabilities = []
    unique_nb_replicas_values = []

    cores_probabilities = []
    unique_cores_values = []

    memory_probabilities = []
    unique_memory_values = []

    #for each of replicas, mem, and cpu, calculate the unique values and their probability distribution
    for i in sorted(nb_replicas):
        if i not in unique_nb_replicas_values:
            nb_replicas_probabilities.append(nb_replicas.count(i)/counter)
            unique_nb_replicas_values.append(i)
    
    for j in sorted(cores):
        if j not in unique_cores_values:
            cores_probabilities.append(cores.count(j)/counter)
            unique_cores_values.append(j)

    for k in sorted(memory):
        if k not in unique_memory_values:
            memory_probabilities.append(memory.count(k)/counter)
            unique_memory_values.append(k)

    

    return unique_nb_replicas_values, nb_replicas_probabilities, unique_cores_values, cores_probabilities, unique_memory_values, memory_probabilities