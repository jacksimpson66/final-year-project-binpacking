import csv

data = {
    "small" : {
        "FF" : {},
        "FFD_Max" : {},
        "BFD_Max" : {},
        "FFD_Fitness" : {},
        "WFD-SpreadBinarySearch_Avg" : {},
        "WFD_SpreadRefine_Avg2" : {},
        "ANM-Strict_DotProduct" : {},
        "ANM-Strict_NormalisedDotProduct" : {},
        "ANM-Strict_DynamicNormDotProduct" : {},
        "ANM-Strict_L2Norm" : {},
        "ANM-IterativeNodeActivation_TightFill" : {},
        "ANM-IterativeNodeActivation_DotProduct" : {},
        "ANM-IterativeNodeActivation_NormalisedDotProduct" : {},
        "ANM-IterativeNodeActivation_DynamicNormDotProduct" : {},
        "ANM-IterativeNodeActivation_L2Norm" : {},
        "ANM-IterativeNodeActivation_TightFill" : {}
    },
    "medium" : {
        "FF" : {},
        "FFD_Max" : {},
        "BFD_Max" : {},
        "FFD_Fitness" : {},
        "WFD-SpreadBinarySearch_Avg" : {},
        "WFD_SpreadRefine_Avg2" : {},
        "ANM-Strict_DotProduct" : {},
        "ANM-Strict_NormalisedDotProduct" : {},
        "ANM-Strict_DynamicNormDotProduct" : {},
        "ANM-Strict_L2Norm" : {},
        "ANM-IterativeNodeActivation_TightFill" : {},
        "ANM-IterativeNodeActivation_DotProduct" : {},
        "ANM-IterativeNodeActivation_NormalisedDotProduct" : {},
        "ANM-IterativeNodeActivation_DynamicNormDotProduct" : {},
        "ANM-IterativeNodeActivation_L2Norm" : {},
        "ANM-IterativeNodeActivation_TightFill" : {}
    },
    "large" : {
        "FF" : {},
        "FFD_Max" : {},
        "BFD_Max" : {},
        "FFD_Fitness" : {},
        "WFD-SpreadBinarySearch_Avg" : {},
        "WFD_SpreadRefine_Avg2" : {},
        "ANM-Strict_DotProduct" : {},
        "ANM-Strict_NormalisedDotProduct" : {},
        "ANM-Strict_DynamicNormDotProduct" : {},
        "ANM-Strict_L2Norm" : {},
        "ANM-IterativeNodeActivation_TightFill" : {},
        "ANM-IterativeNodeActivation_DotProduct" : {},
        "ANM-IterativeNodeActivation_NormalisedDotProduct" : {},
        "ANM-IterativeNodeActivation_DynamicNormDotProduct" : {},
        "ANM-IterativeNodeActivation_L2Norm" : {},
        "ANM-IterativeNodeActivation_TightFill" : {}
    }
}

#collect csv data and put into dict
with open('C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\data\\ru_csv.csv') as in_file:
        read_tsv = csv.reader(in_file, delimiter=',')
        header = next(read_tsv)
        for row in read_tsv:
            if row[1] == "100":
                if row[8] not in data["small"]:
                    data["small"][row[8]] = {
                        "sol_0" : [],
                        "time_0" : [],
                        "sol_1" : [],
                        "time_1" : [],
                        "sol_intra" : [],
                        "time_intra" : [],
                        "sol_5" : [],
                        "time_5" : [],
                        "sol_10" : [],
                        "time_10" : []
                    }
                if row[2] == "0":
                    data["small"][row[8]]["sol_0"].append(row[10])
                    data["small"][row[8]]["time_0"].append(row[11])
                if row[2] == "1":
                    data["small"][row[8]]["sol_1"].append(row[10])
                    data["small"][row[8]]["time_1"].append(row[11])
                if row[2] == "intra":
                    data["small"][row[8]]["sol_intra"].append(row[10])
                    data["small"][row[8]]["time_intra"].append(row[11])
                if row[2] == "5":
                    data["small"][row[8]]["sol_5"].append(row[10])
                    data["small"][row[8]]["time_5"].append(row[11])
                if row[2] == "10":
                    data["small"][row[8]]["sol_10"].append(row[10])
                    data["small"][row[8]]["time_10"].append(row[11])

            if row[1] == "250":
                if row[8] not in data["medium"]:
                    data["medium"][row[8]] = {
                        "sol_0" : [],
                        "time_0" : [],
                        "sol_1" : [],
                        "time_1" : [],
                        "sol_intra" : [],
                        "time_intra" : [],
                        "sol_5" : [],
                        "time_5" : [],
                        "sol_10" : [],
                        "time_10" : []
                    }
                if row[2] == "0":
                    data["medium"][row[8]]["sol_0"].append(row[10])
                    data["medium"][row[8]]["time_0"].append(row[11])
                if row[2] == "1":
                    data["medium"][row[8]]["sol_1"].append(row[10])
                    data["medium"][row[8]]["time_1"].append(row[11])
                if row[2] == "intra":
                    data["medium"][row[8]]["sol_intra"].append(row[10])
                    data["medium"][row[8]]["time_intra"].append(row[11])
                if row[2] == "5":
                    data["medium"][row[8]]["sol_5"].append(row[10])
                    data["medium"][row[8]]["time_5"].append(row[11])
                if row[2] == "10":
                    data["medium"][row[8]]["sol_10"].append(row[10])
                    data["medium"][row[8]]["time_10"].append(row[11])

            if row[1] == "500":
                if row[8] not in data["large"]:
                    data["large"][row[8]] = {
                        "sol_0" : [],
                        "time_0" : [],
                        "sol_1" : [],
                        "time_1" : [],
                        "sol_intra" : [],
                        "time_intra" : [],
                        "sol_5" : [],
                        "time_5" : [],
                        "sol_10" : [],
                        "time_10" : []
                    }
                if row[2] == "0":
                    data["large"][row[8]]["sol_0"].append(row[10])
                    data["large"][row[8]]["time_0"].append(row[11])
                if row[2] == "1":
                    data["large"][row[8]]["sol_1"].append(row[10])
                    data["large"][row[8]]["time_1"].append(row[11])
                if row[2] == "intra":
                    data["large"][row[8]]["sol_intra"].append(row[10])
                    data["large"][row[8]]["time_intra"].append(row[11])
                if row[2] == "5":
                    data["large"][row[8]]["sol_5"].append(row[10])
                    data["large"][row[8]]["time_5"].append(row[11])
                if row[2] == "10":
                    data["large"][row[8]]["sol_10"].append(row[10])
                    data["large"][row[8]]["time_10"].append(row[11])

#averaging values and putting into latex table format in file

with open("C:\\Users\\Craig Simpson\\OneDrive\\Final Year Comp Sci\\FYP\\repo\\final-year-project-binpacking\\data_input\\output\\ru_latex.txt", "w") as f:
    for size in data:
        f.write(size)
        f.write("\n")
        for algo in data[size]:
            f.write(algo)
            for result in data[size][algo]:
                f.write(" & ")
                if len(data[size][algo][result]) > 0:
                    total = 0
                    count = 0
                    for i in data[size][algo][result]:
                        total += float(i)
                        count += 1
                    f.write(str(round((total/count),2)))
                else:
                    f.write("N/A")
            f.write("\n")
