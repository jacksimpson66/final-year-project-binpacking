cmake_minimum_required(VERSION 3.2)
#Sets the name of the project, and stores it in the variable PROJECT_NAME. 
#When called from the top-level CMakeLists.txt also stores the project name in the 
#variable CMAKE_PROJECT_NAME.

# Also sets the variables:

# PROJECT_SOURCE_DIR, <PROJECT-NAME>_SOURCE_DIR
# Absolute path to the source directory for the project.
project("Binpack" CXX)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")# -lpthread")

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR})

enable_testing()

# Get the binpack lib
add_subdirectory(src/Binpack_lib)


# Get the problem A1 bin packing executables
add_subdirectory(src/problem_A1)

add_subdirectory(src/unit_tests)
