## CPACP Jack Simpson Final Year Project - Optimisation Algorithms for Long-Running Application Capacity Planning in Production Clusters

To re create the experiments performed in this project:

- Clone this project
- Run the Python script in /data_input/ called generation_script.py with parameters you want
- Then use CMAKE to build the executables
- Running Time experiment can be performed using time_complexity executable
- Resource Utilisation experiment can be performed using resource_utilisation executable
- Placement Quality experiment can be performed using placement_quality executable!

For the experiment executables and generating the input instances please change the input/output file path variables as appropriate.

Thanks!

Jack Simpson
22/04/22
